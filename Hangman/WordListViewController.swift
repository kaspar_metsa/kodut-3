

import UIKit

class WordListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var allWords : [String]!
    var classifiedWords : [String:[String]]!
    var classifiedWordsKeys = [String]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = NSBundle.mainBundle().pathForResource("words", ofType: "plist") // Get path of words file
        
        //as! does forced conversion from NSArray type to String array type. ! indicates that conversion might fail.
        allWords = NSArray(contentsOfFile: path!) as! [String] // get array of words from file
        
        classifiedWords = classifyWords(allWords) // classify words according to character count
        classifiedWordsKeys.sortInPlace() // sort words according to character count
        tableView.reloadData() // Load data in tableview
        self.navigationController?.setNavigationBarHidden(false, animated: true) // Display Navigation bar on top because of back button
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func classifyWords(words: [String]) -> [String:[String]] { // This function classifys words according to character count
        
        var classifiedWords = [String:[String]]() // Dictionary to store results
        
        
        for word in words {
            let key = word.characters.count > 9 ? "\(word.characters.count) characters word" : "0\(word.characters.count) characters word" // Create key for word
            if classifiedWords[key] == nil { // check if a value against key is present or not
                classifiedWords[key] = [String]() // create array for key if not already present
            }
            classifiedWords[key]!.append(word) // add word in array against key
            
            if !classifiedWordsKeys.contains(key) {
                classifiedWordsKeys.append(key) // Storing key in separate array
            }
        }
        
        
        return classifiedWords
    }
    
}

extension WordListViewController: UITableViewDelegate, UITableViewDataSource { // Extension to separate tableview methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return classifiedWords.count // number of sections equal to key/value pairs present in dictionary
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true) // just deselect the selected row
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classifiedWords[classifiedWordsKeys[section]]!.count // number of rows to be display equal to number of words present
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("wordCell") // dequeue cell from tableview
        cell?.textLabel?.text = classifiedWords[classifiedWordsKeys[indexPath.section]]![indexPath.row] // shows word in cell
        return cell!
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return classifiedWordsKeys[section]// shows tableview header
    }
}
