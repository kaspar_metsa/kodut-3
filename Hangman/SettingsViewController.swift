

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var stepper: UIStepper! // stepper for character count update
    @IBOutlet weak var lblCount: UILabel! // label to show current character count
    override func viewDidLoad() {
        super.viewDidLoad()

        let path = NSBundle.mainBundle().pathForResource("words", ofType: "plist") // Get path of words file
        let allWords = NSArray(contentsOfFile: path!) as! [String] // get array of words from file
        
        var min = 100
        var max = 0
        
        
        
        for w in allWords { // calculating max and min values of stepper
            if w.characters.count < min {
                min = w.characters.count
            }
            if w.characters.count > max {
                max = w.characters.count
            }
        }
        
        //setting max and min values of stepper
        stepper.minimumValue = Double(min)
        stepper.maximumValue = Double(max)
        
        stepper.value = Double(Settings.sharedSettings.charCount) // setting stepper value
        
        stepperValueChanged(stepper) // updating value of label
        
        self.navigationController?.setNavigationBarHidden(false, animated: true) // show navigation bar
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func stepperValueChanged(sender: AnyObject) {
        lblCount.text = "\(stepper.value)"
        Settings.sharedSettings.charCount = Int(stepper.value)
    }

}
