

import UIKit

class HangmanViewController: UIViewController {
    
    // outlets for controls in ViewController
    //"You won"
    @IBOutlet weak var lblResult: UILabel!
    //"a","b","c" etc
    @IBOutlet weak var collectionView: UICollectionView!
    //"c-mmunity", the word with dashes
    @IBOutlet weak var lblWord: UILabel!
    @IBOutlet weak var btnRestart: UIButton!
    //"a","b","c" etc, the characters that were wrong guesses
    @IBOutlet var lblMistakes: [UILabel]!
    @IBOutlet var imgHangman: UIImageView!
    
    let maxMistakes = 9 // max number of mistakes
    var currentMistakes = 0 // current number of mistakes
    
    var indexPath : NSIndexPath!
    var wordsToUse = [String]() //remaining words to change
    var wordIndex = 0 // current word index
    var allWords = [String]() // allWords for choice
    var classifiedWords = [Int:[String]]() // classified words for character count
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true) // hide navigation bar
        
        let path = NSBundle.mainBundle().pathForResource("words", ofType: "plist") // path for verb file
        allWords = NSArray(contentsOfFile: path!) as! [String] // array of words for file
        classifiedWords = classifyWords(allWords) // classify words
        
        // Customizing Restart Button
        btnRestart.layer.cornerRadius = 5
        btnRestart.layer.borderWidth = 0.5
        btnRestart.layer.borderColor = UIColor.whiteColor().CGColor
        
        reset() // setup UI
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reset() {
        imgHangman.image = UIImage(named: "hangman1") // resetting image to first image
        imgHangman.hidden = true // hides hangman
        btnRestart.hidden = true // hides reset button
        
        for lbl in lblMistakes { // empties mistakes labels
            lbl.text = ""
        }
        
        currentMistakes = 0 // resets current mistakes
        wordsToUse = (classifiedWords[Settings.sharedSettings.charCount]?.shuffle())! // words in random order to be used in game
        //Take the first random word that has specified number of characters
        lblWord.text = getDashesForString(wordsToUse.first!) // setting dashes for word
        lblResult.text = "" // hiding Win/Lose Label
        
        collectionView.userInteractionEnabled = true // enable collection view for character selection
        
        print("Start with word \(wordsToUse.first!)") // printing first word selected
        
    }
    
    
    func getDashesForString(word: String) -> String { //creates a dashes string
        var str = ""
        
        for _ in 0..<word.characters.count {
            str += "-"
        }
        return str
        
    }
    
    // replaces characters in answer string
    func getStringByReplacingDashesForCharacter(character: String, dashedString: String, OrignalString:String) -> String {
        
        var dString = dashedString // current answer string
        
        var i = 0
        for char in OrignalString.characters { // check each character in correct answer
            if char == character.characters.first { // check if character is equal to selected character
                
                var srt : NSString = dString // converting to NSString
                srt = srt.stringByReplacingCharactersInRange(NSMakeRange(i, 1), withString: character) // replacing character range
                dString = srt as String // convert back to string
                
//                let total = wordsToUse.count // total words in use
//                var words = wordsToUse // filter array to filter words in use
//                
////                for ww in (wordIndex..<total).reverse() { // loop in reverse order
////                    
////                    let w : NSString = words[ww] // convert to NSString
////                    let char : NSString = character // convert to NSString
////                    if w.characterAtIndex(i) != char.characterAtIndex(0) {
////                        wordsToUse.removeAtIndex(ww) // remove word if it is not similar to correct character word
////                    }
////                }
            }
            i += 1 // character index
        }
        return dString
    }
    
    func didEnterAlphabet(alphabet: String) { // when user enters an alphabet
        
        let smallChar = alphabet.lowercaseString // convert to lowercase
        
        if wordsToUse[wordIndex].containsString(smallChar) { // if correct character (Go for cheat :D)
            if wordIndex < wordsToUse.count-1 { // if current word is not last (Definetly Go for cheat :D)
                wordIndex += 1 // Move to next word
                print("Word changed to: \(wordsToUse[wordIndex])") // print next word
                lblWord.text = getDashesForString(wordsToUse[wordIndex])
            }
        }
        
        if wordsToUse[wordIndex].containsString(smallChar) { // check if word contains entered alphabet
            
            var i = 0 // start index of characters
            for char in wordsToUse[wordIndex].characters { // iterate through each character
                if char == smallChar.characters.first { // if correct character
                    lblWord.text = getStringByReplacingDashesForCharacter(smallChar, dashedString: lblWord.text!, OrignalString: wordsToUse[wordIndex]) // replace character in answer string (Dashed String)
                }
                i += 1 // onto next character
            }
            
            if !(lblWord.text?.containsString("-"))! { // if all characters guessed
                lblResult.text = "You Won" // You Won :(
                btnRestart.hidden = false // show reset button
                
                collectionView.userInteractionEnabled = false // disable access to character collection view
            }
        }
        else {
                madeMistake(alphabet) // Wrong entry :D
        }
    }
    
    func madeMistake(alphabet: String) { // On mistake
        lblMistakes[currentMistakes].text = alphabet // show alphabet for mistake
        currentMistakes += 1 // update current number of mistakes
        imgHangman.hidden = false // show hangman image
        imgHangman.image = UIImage(named: "hangman\(currentMistakes)")// update hangman image for number of mistakes
        if currentMistakes == maxMistakes { // if user made max number of mistakes
            lblResult.text = "You Lose" // Show You Lose
            btnRestart.hidden = false // show reset button
            collectionView.userInteractionEnabled = false // disable alphabets collection view
        }

    }
    
    func classifyWords(words: [String]) -> [Int:[String]] { // This function classifys words according to character count
        
        var classifiedWords = [Int:[String]]() // Dictionary to store results
        
        
        for word in words {
            if classifiedWords[word.characters.count] == nil {  // check if a value against key is present or not
                classifiedWords[word.characters.count] = [String]() // create array for key if not already present
            }
            classifiedWords[word.characters.count]!.append(word) // add word in array against key
            
        }
        
        
        return classifiedWords
    }
    

    @IBAction func btnRestartPressed(sender: AnyObject) {
        reset() // Reset game state
    }

    @IBAction func btnBackPressed(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true) // Back to Main Screen
    }
}

extension HangmanViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 26 // number of alphabets
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if indexPath.item == 0 {
            self.indexPath = indexPath // to dequeue reusable cell for first index path
        }
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("alphabetCell", forIndexPath: self.indexPath) as! AlphabetCell
        
        //65 is "A"
        let ascii = 65 + indexPath.row // character ascii
        
        let c = Character(UnicodeScalar(ascii)) // character from ascii
        
        cell.lblAlphabet.text = "\(c)" // show character
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {

        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        let ascii = 65 + indexPath.row
        let c = Character(UnicodeScalar(ascii)) // selected character

        didEnterAlphabet("\(c)") // perform operation according to character selected
    }
}

//SOURCE: https://gist.github.com/natecook1000/ef096622dab1981823c5
extension CollectionType {
    /// Return a copy of `self` with its elements shuffled
    func shuffle() -> [Generator.Element] {
        var list = Array(self)
        list.shuffleInPlace()
        return list
    }
}

extension MutableCollectionType where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffleInPlace() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in 0..<count - 1 {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            guard i != j else { continue }
            swap(&self[i], &self[j])
        }
    }
}

